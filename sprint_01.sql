-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema i_garden
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema i_garden
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `i_garden` DEFAULT CHARACTER SET utf8mb4 ;
USE `i_garden` ;

-- -----------------------------------------------------
-- Table `i_garden`.`famille_genre_variete`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden`.`famille_genre_variete` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NULL,
  `famille_genre_variete_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_famille_genre_variete_famille_genre_variete1_idx` (`famille_genre_variete_id` ASC) VISIBLE,
  CONSTRAINT `fk_famille_genre_variete_famille_genre_variete1`
    FOREIGN KEY (`famille_genre_variete_id`)
    REFERENCES `i_garden`.`famille_genre_variete` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden`.`classification`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden`.`classification` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden`.`graine`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden`.`graine` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom_latin` VARCHAR(45) NOT NULL,
  `hauteur_max` VARCHAR(45) NULL,
  `photo` VARCHAR(45) NULL,
  `icone` VARCHAR(45) NULL,
  `description` TEXT NULL,
  `famille_genre_variete_id` INT UNSIGNED NULL,
  `classification_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_graine_famille1_idx` (`famille_genre_variete_id` ASC) VISIBLE,
  INDEX `fk_graine_classification1_idx` (`classification_id` ASC) VISIBLE,
  CONSTRAINT `fk_graine_famille1`
    FOREIGN KEY (`famille_genre_variete_id`)
    REFERENCES `i_garden`.`famille_genre_variete` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_graine_classification1`
    FOREIGN KEY (`classification_id`)
    REFERENCES `i_garden`.`classification` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden`.`utilisateur`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden`.`utilisateur` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pseudo` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `mot_de_passe` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden`.`rusticite`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden`.`rusticite` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `zone` VARCHAR(45) NOT NULL,
  `min` VARCHAR(45) NOT NULL,
  `max` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden`.`jardin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden`.`jardin` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  `code_postal` VARCHAR(5) NULL,
  `rusticite_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_jardin_rusticite1_idx` (`rusticite_id` ASC) VISIBLE,
  CONSTRAINT `fk_jardin_rusticite1`
    FOREIGN KEY (`rusticite_id`)
    REFERENCES `i_garden`.`rusticite` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden`.`zone`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden`.`zone` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  `type_sol` VARCHAR(45) NULL,
  `description` TEXT NULL,
  `jardin_id` INT UNSIGNED NOT NULL,
  `rusticite_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_zone_jardin1_idx` (`jardin_id` ASC) VISIBLE,
  INDEX `fk_zone_rusticite1_idx` (`rusticite_id` ASC) VISIBLE,
  CONSTRAINT `fk_zone_jardin1`
    FOREIGN KEY (`jardin_id`)
    REFERENCES `i_garden`.`jardin` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_zone_rusticite1`
    FOREIGN KEY (`rusticite_id`)
    REFERENCES `i_garden`.`rusticite` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden`.`cycle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden`.`cycle` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden`.`jardin_has_utilisateur`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden`.`jardin_has_utilisateur` (
  `jardin_id` INT UNSIGNED NOT NULL,
  `utilisateur_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`jardin_id`, `utilisateur_id`),
  INDEX `fk_jardin_has_utilisateur_utilisateur1_idx` (`utilisateur_id` ASC) VISIBLE,
  INDEX `fk_jardin_has_utilisateur_jardin1_idx` (`jardin_id` ASC) VISIBLE,
  CONSTRAINT `fk_jardin_has_utilisateur_jardin1`
    FOREIGN KEY (`jardin_id`)
    REFERENCES `i_garden`.`jardin` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_jardin_has_utilisateur_utilisateur1`
    FOREIGN KEY (`utilisateur_id`)
    REFERENCES `i_garden`.`utilisateur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden`.`plante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden`.`plante` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `graine_id` INT UNSIGNED NOT NULL,
  `zone_id` INT UNSIGNED NULL,
  `cycle_id` INT UNSIGNED NOT NULL,
  `date_semis` DATE NULL,
  `date_repiquage` DATE NULL,
  `date_floraison` DATE NULL,
  `date_recolte` DATE NULL,
  `quantite_recolte` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_plante_has_zone_zone1_idx` (`zone_id` ASC) VISIBLE,
  INDEX `fk_plante_has_zone_plante1_idx` (`graine_id` ASC) VISIBLE,
  INDEX `fk_plante_has_zone_cycle1_idx` (`cycle_id` ASC) VISIBLE,
  CONSTRAINT `fk_plante_has_zone_plante1`
    FOREIGN KEY (`graine_id`)
    REFERENCES `i_garden`.`graine` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_plante_has_zone_zone1`
    FOREIGN KEY (`zone_id`)
    REFERENCES `i_garden`.`zone` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_plante_has_zone_cycle1`
    FOREIGN KEY (`cycle_id`)
    REFERENCES `i_garden`.`cycle` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden`.`statut`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden`.`statut` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden`.`etat`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden`.`etat` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  `date` DATE NULL,
  `statut_id` INT UNSIGNED NOT NULL,
  `plante_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_etat_statut1_idx` (`statut_id` ASC) VISIBLE,
  INDEX `fk_etat_plante_has_zone1_idx` (`plante_id` ASC) VISIBLE,
  CONSTRAINT `fk_etat_statut1`
    FOREIGN KEY (`statut_id`)
    REFERENCES `i_garden`.`statut` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_etat_plante_has_zone1`
    FOREIGN KEY (`plante_id`)
    REFERENCES `i_garden`.`plante` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden`.`historique_cycle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden`.`historique_cycle` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `plante_id` INT UNSIGNED NOT NULL,
  `cycle_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_historique_cycle_plante1_idx` (`plante_id` ASC) VISIBLE,
  INDEX `fk_historique_cycle_cycle1_idx` (`cycle_id` ASC) VISIBLE,
  CONSTRAINT `fk_historique_cycle_plante1`
    FOREIGN KEY (`plante_id`)
    REFERENCES `i_garden`.`plante` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_historique_cycle_cycle1`
    FOREIGN KEY (`cycle_id`)
    REFERENCES `i_garden`.`cycle` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden`.`historique_zone`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden`.`historique_zone` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `plante_id` INT UNSIGNED NOT NULL,
  `zone_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_historique_zone_plante1_idx` (`plante_id` ASC) VISIBLE,
  INDEX `fk_historique_zone_zone1_idx` (`zone_id` ASC) VISIBLE,
  CONSTRAINT `fk_historique_zone_plante1`
    FOREIGN KEY (`plante_id`)
    REFERENCES `i_garden`.`plante` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_historique_zone_zone1`
    FOREIGN KEY (`zone_id`)
    REFERENCES `i_garden`.`zone` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden`.`historique_statut`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden`.`historique_statut` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `plante_id` INT UNSIGNED NOT NULL,
  `statut_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_historique_statut_plante1_idx` (`plante_id` ASC) VISIBLE,
  INDEX `fk_historique_statut_statut1_idx` (`statut_id` ASC) VISIBLE,
  CONSTRAINT `fk_historique_statut_plante1`
    FOREIGN KEY (`plante_id`)
    REFERENCES `i_garden`.`plante` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_historique_statut_statut1`
    FOREIGN KEY (`statut_id`)
    REFERENCES `i_garden`.`statut` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Trigger
-- -----------------------------------------------------
DELIMITER |
CREATE DEFINER = CURRENT_USER TRIGGER `i_garden`.`plante_AFTER_UPDATE` AFTER UPDATE ON `plante` FOR EACH ROW
BEGIN
    IF OLD.cycle_id != NEW.cycle_id
    THEN
        INSERT INTO historique_cycle(`date`,`plante_id`,`cycle_id`) VALUES (current_date(),id, NEW.cycle_id);
    END IF;
END |
DELIMITER ;

DELIMITER //
CREATE TRIGGER on_etat_update AFTER UPDATE on etat for each row 
IF NEW.plante_id != OLD.plante_id THEN
insert into historique_statut (`date`, `plante_id`, `statut_id`) values (current_date(), NEW.plante_id, NEW.statut_id);
END IF //
DELIMITER ;

DELIMITER //
CREATE TRIGGER after_zone_update AFTER UPDATE ON plante FOR EACH ROW
IF NEW.zone_id != OLD.zone_id THEN
INSERT INTO historique_zone(`date`, `plante_id`, `zone_id`) values(current_date(), NEW.id, NEW.zone_id);
END IF //
DELIMITER ;

-- -----------------------------------------------------
-- Data
-- -----------------------------------------------------

INSERT INTO `classification` (`id`,`nom`) VALUES (1,'Vivace');
INSERT INTO `classification` (`id`,`nom`) VALUES (2,'Annuelle');
INSERT INTO `classification` (`id`,`nom`) VALUES (3,'Bisannuelle');

INSERT INTO `famille_genre_variete` (`id`,`nom`) VALUES (1,'Solanacée');
INSERT INTO `famille_genre_variete` (`id`,`nom`) VALUES (2,'Cucurbitacée');
INSERT INTO `famille_genre_variete` (`id`,`nom`) VALUES (3,'Fabacée');

INSERT INTO `graine` (`id`,`nom_latin`,`classification_id`,`famille_genre_variete_id`) VALUES (1,'Solanum lycopersicum',2,1);
INSERT INTO `graine` (`id`,`nom_latin`,`classification_id`,`famille_genre_variete_id`) VALUES (2,'Cucurbitaceae',2,2);
INSERT INTO `graine` (`id`,`nom_latin`,`classification_id`,`famille_genre_variete_id`) VALUES (3,'Phaseolus vulgaris',2,3);

INSERT INTO `statut` (`id`,`nom`) VALUES (1,'Actif');
INSERT INTO `statut` (`id`,`nom`) VALUES (2,'Passé');
INSERT INTO `statut` (`id`,`nom`) VALUES (3,'Traité');

INSERT INTO `cycle` (`id`,`nom`) VALUES (1,'Germination');
INSERT INTO `cycle` (`id`,`nom`) VALUES (2,'Croissance');
INSERT INTO `cycle` (`id`,`nom`) VALUES (3,'Floraison');
INSERT INTO `cycle` (`id`,`nom`) VALUES (4,'Fructification');
INSERT INTO `cycle` (`id`,`nom`) VALUES (5,'Mort');

INSERT INTO `rusticite` (`id`,`zone`,`min`,`max`) VALUES (1,'1','-273.15','-45');
INSERT INTO `rusticite` (`id`,`zone`,`min`,`max`) VALUES (2,'2','-45','-40');
INSERT INTO `rusticite` (`id`,`zone`,`min`,`max`) VALUES (3,'3','-40','-34');
INSERT INTO `rusticite` (`id`,`zone`,`min`,`max`) VALUES (4,'4','-34','-29');
INSERT INTO `rusticite` (`id`,`zone`,`min`,`max`) VALUES (5,'5','-29','-23');
INSERT INTO `rusticite` (`id`,`zone`,`min`,`max`) VALUES (6,'6','-23','-18');
INSERT INTO `rusticite` (`id`,`zone`,`min`,`max`) VALUES (7,'7','-18','-12');

INSERT INTO `utilisateur` (`id`,`pseudo`,`email`,`mot_de_passe`) VALUES (1,'samuel','samuel@epsi.fr','0000');
INSERT INTO `utilisateur` (`id`,`pseudo`,`email`,`mot_de_passe`) VALUES (2,'valentin','valentin@epsi.fr','0000');
INSERT INTO `utilisateur` (`id`,`pseudo`,`email`,`mot_de_passe`) VALUES (3,'erwann','erwann@epsi.fr','0000');

INSERT INTO `jardin` (`id`,`nom`,`code_postal`,`rusticite_id`) VALUES (1,'Bintinais','35000',1);
INSERT INTO `jardin` (`id`,`nom`,`code_postal`,`rusticite_id`) VALUES (2,'Prévalaye','35000',1);
INSERT INTO `jardin` (`id`,`nom`,`code_postal`,`rusticite_id`) VALUES (3,'Sainte-Foix','35000',1);
INSERT INTO `jardin` (`id`,`nom`,`code_postal`,`rusticite_id`) VALUES (4,'Patis-Tatelin','35000',1);
INSERT INTO `jardin` (`id`,`nom`,`code_postal`,`rusticite_id`) VALUES (5,'Gayeulles','35000',1);

INSERT INTO `jardin_has_utilisateur` (`jardin_id`,`utilisateur_id`) VALUES (1,1);
INSERT INTO `jardin_has_utilisateur` (`jardin_id`,`utilisateur_id`) VALUES (2,2);
INSERT INTO `jardin_has_utilisateur` (`jardin_id`,`utilisateur_id`) VALUES (3,2);

INSERT INTO `zone` (`id`,`nom`,`type_sol`,`description`,`jardin_id`,`rusticite_id`) VALUES (1,'Parcelle 100','Argileuse','Parcelle 100',1,1);
INSERT INTO `zone` (`id`,`nom`,`type_sol`,`description`,`jardin_id`,`rusticite_id`) VALUES (2,'Parcelle 101','Argileuse','Parcelle 101',1,1);
INSERT INTO `zone` (`id`,`nom`,`type_sol`,`description`,`jardin_id`,`rusticite_id`) VALUES (3,'Parcelle 102','Argileuse','Parcelle 102',1,1);
INSERT INTO `zone` (`id`,`nom`,`type_sol`,`description`,`jardin_id`,`rusticite_id`) VALUES (4,'Parcelle 103','Argileuse','Parcelle 103',1,1);
INSERT INTO `zone` (`id`,`nom`,`type_sol`,`description`,`jardin_id`,`rusticite_id`) VALUES (5,'Parcelle 201','Argileuse','Parcelle 201',2,1);
INSERT INTO `zone` (`id`,`nom`,`type_sol`,`description`,`jardin_id`,`rusticite_id`) VALUES (6,'Parcelle 202','Argileuse','Parcelle 202',2,1);
INSERT INTO `zone` (`id`,`nom`,`type_sol`,`description`,`jardin_id`,`rusticite_id`) VALUES (7,'Parcelle 203','Argileuse','Parcelle 203',2,1);
INSERT INTO `zone` (`id`,`nom`,`type_sol`,`description`,`jardin_id`,`rusticite_id`) VALUES (8,'Parcelle 301','Argileuse','Parcelle 301',3,1);
INSERT INTO `zone` (`id`,`nom`,`type_sol`,`description`,`jardin_id`,`rusticite_id`) VALUES (9,'Parcelle 302','Argileuse','Parcelle 302',3,1);
INSERT INTO `zone` (`id`,`nom`,`type_sol`,`description`,`jardin_id`,`rusticite_id`) VALUES (10,'Parcelle 303','Argileuse','Parcelle 303',3,1);

INSERT INTO `plante` (`id`,`graine_id`,`zone_id`,`cycle_id`, `date_semis`) VALUES (1,1,1,2,"2022-03-01");
INSERT INTO `plante` (`id`,`graine_id`,`zone_id`,`cycle_id`, `date_semis`) VALUES (2,2,1,1,"2022-04-18");
INSERT INTO `plante` (`id`,`graine_id`,`zone_id`,`cycle_id`, `date_semis`) VALUES (3,1,2,2,"2022-03-01");
INSERT INTO `plante` (`id`,`graine_id`,`zone_id`,`cycle_id`, `date_semis`) VALUES (4,2,2,1,"2022-04-18");
INSERT INTO `plante` (`id`,`graine_id`,`zone_id`,`cycle_id`, `date_semis`) VALUES (5,1,5,2,"2022-03-01");
INSERT INTO `plante` (`id`,`graine_id`,`zone_id`,`cycle_id`, `date_semis`) VALUES (6,2,5,1,"2022-04-18");
INSERT INTO `plante` (`id`,`graine_id`,`zone_id`,`cycle_id`, `date_semis`) VALUES (7,1,8,2,"2022-03-01");
INSERT INTO `plante` (`id`,`graine_id`,`cycle_id`, `date_semis`) VALUES (8,2,1,"2022-04-18");

-- -----------------------------------------------------
-- View Liste_des_plantes
-- -----------------------------------------------------
create view Liste_des_plantes as
    select graine.nom_latin, zone.nom as zone_nom, cycle.nom
    from graine
    join plante on graine.id = plante.graine_id
    join zone on plante.zone_id = zone.id
    join cycle on cycle.id = plante.cycle_id;

-- -----------------------------------------------------
-- View Zones_recap
-- -----------------------------------------------------
create view Zones_recap as
    select zone.nom, rusticite.zone, count(plante.id)
    from zone
    join plante on zone.id = plante.zone_id
    join rusticite on rusticite.id = zone.rusticite_id
    group by zone.nom;

-- -----------------------------------------------------
-- View Plantes_hors_zone
-- -----------------------------------------------------
create view Plantes_hors_zone as
    select g.nom_latin
    from plante
    join graine g on plante.graine_id = g.id
    where plante.zone_id is null ;

-- -----------------------------------------------------
-- View Plantes_recap
-- -----------------------------------------------------
CREATE VIEW plantes_recap AS
    SELECT graine.nom_latin, CONCAT(ucase(LEFT(famille_genre_variete.nom, 1)), LCASE(substring(famille_genre_variete.nom, 2)))
    FROM graine
    JOIN famille_genre_variete ON ((famille_genre_variete.famille_genre_variete_id = graine.famille_genre_variete_id));