USE `i_garden` ;
-- -----------------------------------------------------
-- Update database
-- -----------------------------------------------------

SET autocommit = OFF;
START TRANSACTION;

ALTER TABLE `i_garden`.`historique_statut`
    ADD `etat_id` INT UNSIGNED NOT NULL,
    ADD CONSTRAINT `fk_historique_statut_etat1`
        FOREIGN KEY (`etat_id`)
            REFERENCES `i_garden`.`etat` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    ADD INDEX `fk_historique_statut_etat1_idx` (`etat_id` ASC) VISIBLE,
    DROP COLUMN `statut_id`,
    DROP INDEX `fk_historique_statut_statut1_idx`,
    DROP CONSTRAINT `fk_historique_statut_statut1`
;

ALTER TABLE `i_garden`.`etat`
    DROP COLUMN `statut_id`,
    DROP INDEX `fk_etat_statut1_idx`,
    DROP CONSTRAINT `fk_etat_statut1`,
    ADD `statut` ENUM('Actif', 'Passé', 'Traité')
;

DROP TABLE `i_garden`.`statut`;

COMMIT;
SET autocommit = ON;

-- -----------------------------------------------------
-- Update trigger
-- -----------------------------------------------------

DROP TRIGGER IF EXISTS on_etat_update;

DELIMITER //
CREATE TRIGGER on_etat_update AFTER UPDATE on etat for each row
    IF NEW.plante_id != OLD.plante_id THEN
        INSERT INTO historique_statut (`date`, `plante_id`, `etat_id`) values (current_date(), NEW.plante_id, NEW.id);
    END IF //
DELIMITER ;

-- -----------------------------------------------------
-- Update database
-- -----------------------------------------------------

SET autocommit = OFF;
START TRANSACTION;

CREATE TABLE IF NOT EXISTS `i_garden`.`note` (
     id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
     zone_id INT UNSIGNED,
     plante_id INT UNSIGNED,
     graine_id INT UNSIGNED,
     type enum('texte','audio') not null,
     description TEXT,
     lien_audio VARCHAR(255),
     date_creation DATE NOT NULL DEFAULT (DATE_FORMAT(NOW(), '%Y-%m-%d')),
     INDEX `fk_note_zone1_idx` (`zone_id` ASC) VISIBLE,
     INDEX `fk_note_plante1_idx` (`plante_id` ASC) VISIBLE,
     INDEX `fk_note_graine1_idx` (`graine_id` ASC) VISIBLE,
     CONSTRAINT check_texte CHECK (type='texte' and description is not null),
     CONSTRAINT check_audio CHECK (type='audio' and lien_audio is not null),
     CONSTRAINT `fk_note_zone1`
        FOREIGN KEY (zone_id) REFERENCES `i_garden`.`zone` (`id`),
     CONSTRAINT `fk_note_plante1`
         FOREIGN KEY (plante_id) REFERENCES `i_garden`.`plante` (`id`)
         ON UPDATE CASCADE
         ON DELETE CASCADE,
     CONSTRAINT `fk_note_graine1`
         FOREIGN KEY (graine_id) REFERENCES `i_garden`.`graine` (`id`)
);

ALTER TABLE `i_garden`.`zone`
    ADD `latitude` DECIMAL(7,6),
    ADD `longitude` DECIMAL(7,6),
    ADD `largeur` TINYINT UNSIGNED,
    ADD `hauteur` TINYINT UNSIGNED
;

CREATE TABLE type_action
(
id int unsigned primary key not null auto_increment,
type_parent_id int unsigned,
nom varchar(255) not null,
description_action TEXT not null,
logo_url varchar(255),
CONSTRAINT fk_type_parent
foreign key (type_parent_id)
references type_action(id)
	ON UPDATE CASCADE
    ON DELETE CASCADE
);


CREATE TABLE action
(
id int unsigned primary key not null AUTO_INCREMENT,
utilisateur_id int unsigned not null,
type_action_id int unsigned,
frequence float(6) unsigned,
date_debut date,
date_fin date,
statut enum("A faire", "Réalisé", "Abandonné") not null,
CONSTRAINT fk_type_action
FOREIGN KEY (type_action_id)
references type_action(id)
	ON UPDATE CASCADE
    ON DELETE CASCADE,
CONSTRAINT fk_utilisation
FOREIGN KEY (utilisateur_id)
references utilisateur(id)
	ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE historique_action
(
`id` int unsigned primary key not null auto_increment,
`date` date not null,
`action_id` int unsigned not null,
`statut` enum("A faire", "Réalisé", "Abandonné") not null,
CONSTRAINT fk_action_id
foreign key (`action_id`)
references action(`id`)
);

DROP TRIGGER IF EXISTS after_statut_action_update;

DELIMITER //
CREATE TRIGGER after_statut_action_update
after update on action for each row
BEGIN
	IF OLD.statut != NEW.statut THEN
		INSERT INTO historique_action(`date`, action_id, statut)
        VALUES(now(), NEW.id, NEW.statut);
	END IF;
END//
DELIMITER ;

CREATE TABLE IF NOT EXISTS `i_garden`.`nom_graine` (
     id INT UNSIGNED NOT NULL AUTO_INCREMENT,
     nom VARCHAR(45),
     graine_id INT UNSIGNED,
     PRIMARY KEY (`id`),
     INDEX `fk_nom_graine1_idx` (`graine_id` ASC) VISIBLE,
     CONSTRAINT `fk_nom_graine1`
         FOREIGN KEY (graine_id) REFERENCES `i_garden`.`graine` (`id`)
);

DROP PROCEDURE IF EXISTS nom_graine;

DELIMITER |
CREATE PROCEDURE nom_graine()
BEGIN
    DECLARE v_nom VARCHAR(45);
    DECLARE v_id INT;
    DECLARE fin BOOLEAN DEFAULT FALSE;
    DECLARE curs_graines CURSOR
        FOR SELECT id, nom_latin
            FROM `i_garden`.`graine`;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin = TRUE;
    OPEN curs_graines;
    loop_curseur: LOOP
        FETCH curs_graines INTO v_id, v_nom;
        IF fin THEN
            LEAVE loop_curseur;
        END IF;
        INSERT INTO `i_garden`.`nom_graine` (graine_id, nom) VALUES (v_id,v_nom);
    END LOOP;
    CLOSE curs_graines;
END|
DELIMITER ;

CALL nom_graine();

ALTER TABLE `i_garden`.`graine`
    DROP COLUMN `nom_latin`
;

ALTER TABLE `i_garden`.`plante`
    ADD `temp_mini` TINYINT
;

COMMIT;
SET autocommit = ON;

-- -----------------------------------------------------
-- Update view
-- -----------------------------------------------------

DROP VIEW IF EXISTS Liste_des_plantes;

create view Liste_des_plantes as
select zone.nom as zone_nom, cycle.nom as cycle_nom, ng.nom as graine_nom
from graine
join plante on graine.id = plante.graine_id
join zone on plante.zone_id = zone.id
join cycle on cycle.id = plante.cycle_id
join nom_graine ng on graine.id = ng.graine_id
;

DROP VIEW IF EXISTS Plantes_hors_zone;

create view Plantes_hors_zone as
select ng.nom as graine_nom
from plante
join graine g on plante.graine_id = g.id
join nom_graine ng on g.id = ng.graine_id
where plante.zone_id is null ;

DROP VIEW IF EXISTS plantes_recap;

CREATE VIEW plantes_recap AS
SELECT ng.nom as graine_nom, CONCAT(ucase(LEFT(fgv.nom, 1)), LCASE(substring(fgv.nom, 2))) as famille_genre_variete
FROM graine
JOIN famille_genre_variete fgv on graine.famille_genre_variete_id = fgv.id
JOIN nom_graine ng on graine.id = ng.graine_id;

-- -----------------------------------------------------
-- Update trigger
-- -----------------------------------------------------

DROP TRIGGER IF EXISTS after_delete_utilisateur;
DELIMITER //
CREATE TRIGGER after_delete_utilisateur
AFTER DELETE
ON utilisateur FOR EACH ROW
BEGIN
	DECLARE number_jardin int;
    SET number_jardin := (SELECT count(utilisateur_id) from jardin_has_utilisateur where jardin_has_utilisateur.utilisateur_id = OLD.id);
    IF number_jardin = 1 THEN 
		delete jardin.* from jardin 
        join jardin_has_utilisateur on jardin_has_utilisateur.jardin_id = jardin.id
        join utilisateur on jardin_has_utilisateur.utilisateur_id = OLD.id;
    END IF;
END //
DELIMITER ;

-- -----------------------------------------------------
-- Create user
-- -----------------------------------------------------

CREATE USER if not exists 'demo'@'localhost' IDENTIFIED BY 'demo';
GRANT SELECT, SHOW VIEW ON i_garden.* TO 'demo'@'localhost';